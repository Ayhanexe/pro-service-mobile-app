import 'package:flutter/material.dart';
// import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:oawo_car_wash/widgets/circleProgressBar.dart';

class LoadingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LoadingScreenState();
}

class LoadingScreenState extends State with TickerProviderStateMixin {
  AnimationController progressRotationController;
  Animation<double> progressRotationAnimation;

  AnimationController progressValueController;
  Animation<double> progressValueAnimation;

  @override
  void initState() {
    super.initState();
    progressRotationController = AnimationController(
        duration: const Duration(seconds: 300), vsync: this);

    progressValueController =
        AnimationController(duration: const Duration(milliseconds: 3500), vsync: this);

    progressRotationAnimation =
        Tween(begin: 0.0, end: 1.0).animate(progressRotationController);

    progressValueAnimation =
        Tween(begin: 0.0, end: 0.95).animate(progressValueController);

    progressRotationAnimation.addListener(() {
      setState(() {});
    });
    progressValueAnimation.addListener(() {
      setState(() {});
    });
    progressRotationController.forward();
    progressValueController.forward();

    progressRotationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        progressRotationController.repeat();
      } else if (status == AnimationStatus.dismissed) {
        progressRotationController.forward();
      }
    });

    progressValueController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
            Navigator.pop(context);
            Navigator.pushNamed(context, '/login');
      } else if (status == AnimationStatus.dismissed) {
        progressValueController.forward();
      }
    });
  }

  @override
  void dispose() {
    progressRotationController.dispose();
    progressValueController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            color: Color.fromRGBO(255, 207, 42, 1),
            child: SvgPicture.asset(
              'assets/images/splash_screen_icon.svg',
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                child: SizedBox(
                  width: 50,
                  height: 50,
                  child: Transform.rotate(
                    angle: progressRotationAnimation.value * 360,
                    child: CircleProgressBar(
                      backgroundColor: Colors.black45,
                      foregroundColor: Colors.blueGrey[800],
                      value: progressValueAnimation.value,
                      strokeWidth: 4,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
