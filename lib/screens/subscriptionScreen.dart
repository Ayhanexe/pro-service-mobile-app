import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:oawo_car_wash/utilities/tools.dart';

class SubscriptionScreen extends StatefulWidget {
  static final String routeName = "/subscribe";
  @override
  State<StatefulWidget> createState() => SubscriptionScreenState();
}

class SubscriptionScreenState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          SizedBox(
            width: 110.0,
            height: 60.0,
            child: IconButton(
              onPressed: null,
              icon: SvgPicture.asset(
                'assets/images/splash_screen_icon.svg',
              ),
            ),
          ),
        ],
      ),
      body: Container(
        color: Colors.orange,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SizedBox.expand(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(top: 15.0, left: 15.0),
                child: Row(
                  children: [
                    Tools.makeTitle("Sifarişi Tamamla"),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      child: Tools.makeInput(
                        context,
                        "Tam adınız",
                        Icons.person,
                        false,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      child: Tools.makeInput(
                          context,
                          "Şəxsiyyət vəsiqəsi nömrəsi (AZE)",
                          Icons.directions_car,
                          false),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                      child: Tools.makeInput(
                          context,
                          "Avtomobil seriya nömrəsi",
                          Icons.directions_car,
                          false),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 15.0, right: 5.0),
                            child: Text(
                              "Xidmətlər:",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Raleway',
                                fontWeight: FontWeight.w500,
                                fontSize: 15.0,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 15.0),
                            child: _getServices(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static List<String> services = ["Yuma", "Keramik"];
  String dropdownValue = services[0];
  _getServices() {
    return Container(
      height: 30.0,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(17, 17, 31, 1),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: DropdownButton<String>(
        value: dropdownValue,
        icon: Icon(Icons.arrow_downward),
        iconSize: 17,
        elevation: 16,
        style: TextStyle(color: Colors.white),
        dropdownColor: Color.fromRGBO(17, 17, 31, 1),
        underline: Container(
          height: 0,
        ),
        onChanged: (String newValue) {
          setState(() {
            dropdownValue = newValue;
          });
        },
        items: services.map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(
              value,
            ),
          );
        }).toList(),
      ),
    );
  }
}
