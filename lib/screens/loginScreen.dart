import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:oawo_car_wash/utilities/tools.dart';

class LoginScreen extends StatelessWidget {
  static final String routeName = "/login";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text("Giriş"),
        //   centerTitle: true,
        // ),
        body: Stack(
      fit: StackFit.expand,
      children: [
        Container(
          color: Theme.of(context).primaryColor,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  // color: Color.fromRGBO(255, 207, 42, 1),
                  margin: EdgeInsets.symmetric(vertical: 10.0),
                  width: 150,
                  height: 150,
                  child: SvgPicture.asset(
                    'assets/images/login_screen_icon.svg',
                  ),
                ),
                Tools.makeInput(context, "Email", Icons.person, false),
                Tools.makeInput(context, "Şifre", Icons.vpn_key, true),
                Row(
                  children: [
                    Flexible(
                      flex: 2,
                      child: Container(
                        margin: EdgeInsets.only(left: 15.0, right: 5.0),
                        child: Tools.makeButton(context, "Giriş", '/home'),
                      ),
                    ),
                    Flexible(
                      flex: 2,
                      child: Container(
                        margin: EdgeInsets.only(left: 5.0, right: 15.0),
                        child:
                            Tools.makeButton(context, "Qeydiyyat", '/register'),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
