import 'package:flutter/material.dart';
import 'package:oawo_car_wash/widgets/jobList.dart';

class HomeScreen extends StatelessWidget {
  static final String routeName = "/home";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          title: Text("Xidmətlər"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: JobList(),
        ));
  }
}

class NavDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NavDrawerState();
}

class NavDrawerState extends State {
  // onTap(BuildContext context, index) {}
  String currentRoute = "/home";
  @override
  Widget build(BuildContext context) {
    ListTile _makeListTile(
        Icon icon, String title, String route, BuildContext context) {
      return ListTile(
          leading: icon,
          title: Text(title),
          onTap: () {
            Navigator.pop(context);
            if (currentRoute != route) {
              currentRoute = route;
              if (route == "/login") {
                Navigator.pushReplacementNamed(context, route);
              } else {
                Navigator.pushNamed(context, route);
              }
            }
          });
    }

    var drawerList = <ListTile>[
      _makeListTile(
        Icon(Icons.home),
        "Ana səhifə",
        "/home",
        context,
      ),
      _makeListTile(
        Icon(Icons.supervised_user_circle),
        "Profil",
        "/profile",
        context,
      ),
      _makeListTile(
        Icon(Icons.settings),
        "Parametrlər",
        "/settings",
        context,
      ),
      _makeListTile(
        Icon(Icons.exit_to_app),
        "Çıxış",
        "/login",
        context,
      ),
    ];

    return Container(
      width: MediaQuery.of(context).size.width / 1.5,
      child: Drawer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(color: Colors.orange[600]),
              child: Padding(
                padding: EdgeInsets.all(6),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: 60,
                      height: 60,
                      child: CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/profile.jpg')),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Ayxan Abdlayev",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      "ayhan2015007@gmail.com",
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Column(
              children: [
                Container(
                  // color: Colors.red,
                  width: MediaQuery.of(context).size.width,
                  height: (drawerList.length * 70).toDouble(),
                  child: ListView.builder(
                    itemCount: drawerList.length,
                    itemBuilder: (context, index) {
                      return drawerList[index];
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
