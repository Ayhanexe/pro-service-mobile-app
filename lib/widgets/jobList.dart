import 'package:flutter/material.dart';
import 'package:oawo_car_wash/screens/subscriptionScreen.dart';
import 'package:oawo_car_wash/utilities/tools.dart';
import 'package:table_calendar/table_calendar.dart';

class JobList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => JobListState();
}

class JobListState extends State {
  CalendarController _calendarController;
  int selectedTime;
  var times = [
    ['08:30', false],
    ['09:00', false],
    ['09:30', false],
    ['10:00', false],
    ['10:30', false],
    ['11:00', false],
    ['11:30', false],
    ['12:00', false],
    ['12:30', false],
    ['13:00', false],
    ['13:30', false],
    ['14:00', false],
    ['14:30', false],
    ['15:00', false],
    ['15:30', false],
  ];

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.orange,
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SizedBox.expand(
        child: Column(
          children: [
            Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.orange,
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20.0, left: 15.0),
                      child: Row(
                        children: [
                          Tools.makeTitle("Sifarişlər"),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10.0),
                      color: Colors.white,
                      child: _initializeCalendar(),
                    ),
                    Flexible(
                      child:
                      Container(
                        margin: EdgeInsets.all(10.0),
                        child: _makeClockSelector(context),
                      ),
                    ),
                    Container(
                      child: _subscribeButton(context, "QEYDİYYAT", SubscriptionScreen.routeName),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _makeClockSelector(BuildContext context) {
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: times.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 2,
        crossAxisCount: 5,
        crossAxisSpacing: 3.0,
        mainAxisSpacing: 3.0,
      ),
      itemBuilder: (BuildContext context, int index) {
        return ButtonTheme(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7.0),
          ),
          child: FlatButton(
            onPressed: () {
              _makeOthersNull(index);
              _changeColorWithNum(index);
            },
            color: times[index][1] ? Colors.blue : Colors.white,
            child: Text(
              times[index][0],
              style: TextStyle(
                fontSize: 12.0,
                color: times[index][1] ? Colors.white : Colors.black,
              ),
            ),
          ),
        );
      },
    );
  }

  void _makeOthersNull(int index) {
    for (var i = 0; i < times.length; i++) {
      if (i != index) {
        times[i][1] = false;
      }
    }
  }

  void _changeColorWithNum(int index) {
    if (times[index][1]) {
      setState(() {
        times[index][1] = false;
        selectedTime = null;
      });
    } else {
      setState(() {
        times[index][1] = true;
      });
    }
  }

  _subscribeButton(BuildContext context, String title, String route) {
    var screenSize = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(10.0),
      child: ButtonTheme(
        minWidth: screenSize.width,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        height: 50,
        child: FlatButton(
          onPressed: () {
            Navigator.pushNamed(context, route);
          },
          child: Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 20.0,
              fontWeight: FontWeight.w400,
            ),
          ),
          color: Color.fromRGBO(20, 20, 28, 1),
        ),
      ),
    );
  }

  _initializeCalendar() {
    return TableCalendar(
      // initialCalendarFormat: CalendarFormat.week,
      locale: "az",
      calendarController: _calendarController,
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonDecoration: BoxDecoration(
            color: Colors.orange, borderRadius: BorderRadius.circular(15.0)),
        formatButtonTextStyle: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
        ),
        formatButtonShowsNext: false,
      ),
      startingDayOfWeek: StartingDayOfWeek.monday,
      onDaySelected: (data, events) {
        return false;
      },
    );
  }
}
