import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:oawo_car_wash/screens/homeScreen.dart';
import 'package:oawo_car_wash/screens/loginScreen.dart';
import 'package:oawo_car_wash/screens/splashScreen.dart';
import 'package:oawo_car_wash/screens/subscriptionScreen.dart';
// import 'package:oawo_car_wash/screens/splashScreen.dart';
// import 'package:oawo_car_wash/widgets/jobList.dart';

void main() {
  initializeDateFormatting().then((_) =>runApp(OawoCarWashApp()));
}

class OawoCarWashApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pro Wash',
      debugShowCheckedModeBanner:
       false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:  LoadingScreen(),
      routes: <String, WidgetBuilder>{
        '${LoginScreen.routeName}': (BuildContext context) => LoginScreen(),
        '${HomeScreen.routeName}': (BuildContext context) => HomeScreen(),
        '${SubscriptionScreen.routeName}': (BuildContext context) => SubscriptionScreen(),
      },
    );
  }
}


